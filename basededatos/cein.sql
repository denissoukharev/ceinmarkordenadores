-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Sep 04, 2023 at 11:11 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cein`
--

-- --------------------------------------------------------

--
-- Table structure for table `clases`
--

CREATE TABLE `clases` (
  `claseid` int(11) NOT NULL,
  `clase` varchar(25) DEFAULT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `hora_start` varchar(25) DEFAULT NULL,
  `hora_end` varchar(25) DEFAULT NULL,
  `dias` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `clases`
--

INSERT INTO `clases` (`claseid`, `clase`, `curso`, `hora_start`, `hora_end`, `dias`) VALUES
(34, 'mates', 'abc', '08:30', '09:20', 'lunes'),
(35, 'EYA', 'abc', '08:30', '09:20', 'miercoles'),
(37, 'mates', 'agh', '08:30', '16:00', 'lunes'),
(40, 'EYA', 'abc', '12:20', '13:10', 'lunes'),
(41, 'jui', 'abc', '13:10', '14:00', 'lunes'),
(42, 'mates', 'abc', '08:30', '09:20', 'martes'),
(43, 'mates', 'abc', '11:30', '12:20', 'lunes'),
(44, 'ingles', 'agh', '08:30', '16:00', 'martes');

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE `cursos` (
  `cursoid` int(11) NOT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `fecha_start` varchar(25) DEFAULT NULL,
  `fecha_end` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cursos`
--

INSERT INTO `cursos` (`cursoid`, `curso`, `fecha_start`, `fecha_end`) VALUES
(11, 'abc', '', ''),
(13, 'agh', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `horario`
--

CREATE TABLE `horario` (
  `curso` varchar(100) DEFAULT NULL,
  `hora_start` varchar(100) DEFAULT NULL,
  `hora_end` varchar(100) DEFAULT NULL,
  `horarioid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `horario`
--

INSERT INTO `horario` (`curso`, `hora_start`, `hora_end`, `horarioid`) VALUES
('abc', '08:30', '09:20', 6),
('abc', '09:20', '10:10', 7),
('abc', '10:10', '11:00', 8),
('abc', '11:30', '12:20', 9),
('abc', '12:20', '13:10', 10),
('abc', '13:10', '14:00', 11),
('sdf', '08:30', '09:20', 12),
('sdf', '09:20', '10:10', 13),
('agh', '08:30', '16:00', 16);

-- --------------------------------------------------------

--
-- Table structure for table `pcs`
--

CREATE TABLE `pcs` (
  `codigopc` int(11) NOT NULL,
  `serie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pcs`
--

INSERT INTO `pcs` (`codigopc`, `serie`) VALUES
(202, 200),
(301, 300),
(302, 300),
(303, 300),
(304, 300),
(305, 300),
(306, 300),
(307, 300),
(308, 300),
(309, 300),
(310, 300);

-- --------------------------------------------------------

--
-- Table structure for table `registro`
--

CREATE TABLE `registro` (
  `registroid` int(11) NOT NULL,
  `codigopc` int(11) DEFAULT NULL,
  `serie` int(11) DEFAULT NULL,
  `usuario` varchar(25) DEFAULT NULL,
  `clase` varchar(25) DEFAULT NULL,
  `curso` varchar(25) DEFAULT NULL,
  `hora_man_str` varchar(25) DEFAULT NULL,
  `hora_man_end` varchar(25) DEFAULT NULL,
  `hora_tar_str` varchar(25) DEFAULT NULL,
  `hora_tar_end` varchar(25) DEFAULT NULL,
  `manana_libre` varchar(25) DEFAULT NULL,
  `manana_ocupado` varchar(25) DEFAULT NULL,
  `tarde_libre` varchar(25) DEFAULT NULL,
  `tarde_ocupado` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `registro`
--

INSERT INTO `registro` (`registroid`, `codigopc`, `serie`, `usuario`, `clase`, `curso`, `hora_man_str`, `hora_man_end`, `hora_tar_str`, `hora_tar_end`, `manana_libre`, `manana_ocupado`, `tarde_libre`, `tarde_ocupado`) VALUES
(7, 305, 300, 'celia', 'TES (LOU)', 'GAD', '08:30', '09:20', '', 'lunes', NULL, NULL, NULL, NULL),
(11, 310, 300, 'anton', 'TES (LOU)', 'GAD', '08:30', '09:20', '', 'lunes', NULL, NULL, NULL, NULL),
(16, 602, 600, NULL, 'tes', 'abc', '08:30', '09:20', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 607, 600, NULL, 'tes', 'abc', '08:30', '09:20', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 607, 600, NULL, 'tes', 'abc', '08:30', '09:20', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 301, 301, NULL, 'mates', 'mates', '08:30', '09:20', NULL, 'lunes', NULL, NULL, NULL, NULL),
(78, 302, 302, NULL, 'mates', 'mates', '08:30', '09:20', NULL, 'lunes', NULL, NULL, NULL, NULL),
(182, 301, 300, NULL, 'mates', 'abc', '09:20', '10:10', NULL, 'lunes', NULL, NULL, NULL, NULL),
(185, 303, 303, NULL, 'mates', 'abc', '09:20', '10:10', NULL, 'lunes', NULL, NULL, NULL, NULL),
(186, 306, 306, NULL, 'mates', 'abc', '09:20', '10:10', NULL, 'lunes', NULL, NULL, NULL, NULL),
(187, 307, 300, NULL, 'mates', 'abc', '10:10', '11:00', NULL, 'lunes', NULL, NULL, NULL, NULL),
(189, 202, 202, NULL, 'mates', 'abc', '08:30', '09:20', NULL, 'lunes', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clases`
--
ALTER TABLE `clases`
  ADD PRIMARY KEY (`claseid`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`cursoid`);

--
-- Indexes for table `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`horarioid`);

--
-- Indexes for table `pcs`
--
ALTER TABLE `pcs`
  ADD PRIMARY KEY (`codigopc`);

--
-- Indexes for table `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`registroid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clases`
--
ALTER TABLE `clases`
  MODIFY `claseid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `cursoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `horario`
--
ALTER TABLE `horario`
  MODIFY `horarioid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `registro`
--
ALTER TABLE `registro`
  MODIFY `registroid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
