<?php

namespace app\controllers;

use app\models\Cursos;
use app\models\Clases;
use app\models\Registro;
use app\models\Horario;

use app\models\CursosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CursosController implements the CRUD actions for Cursos model.
 */
class CursosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Cursos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CursosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cursos model.
     * @param int $cursoid Cursoid
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cursoid)
    {
        return $this->render('view', [
            'model' => $this->findModel($cursoid),
        ]);
    }

    /**
     * Creates a new Cursos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Cursos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cursoid' => $model->cursoid]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionCreatemodal()
    {
        $model = new Cursos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['/registro/calendario', 'curso' => $model->curso]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cursos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cursoid Cursoid
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cursoid)
    {
        $model = $this->findModel($cursoid);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cursoid' => $model->cursoid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cursos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cursoid Cursoid
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cursoid)
    {
        $this->findModel($cursoid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cursos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cursoid Cursoid
     * @return Cursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cursoid)
    {
        if (($model = Cursos::findOne(['cursoid' => $cursoid])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    public function actionCalendario($curso)
    {
       /* $model = $this->findModel($registroid);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'registroid' => $model->registroid]);
        }*/
        $horas=Horario::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $clases=Clases::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $pcs=Registro::find()->where(['curso'=>$curso])->orderBy(['hora_man_str'=>'ASC'])->all();
        $cursos=Cursos::find()->all();
        
        if( isset($clases[0]->hora_start) && $clases[0]->hora_start=="08:30"){
        return $this->render('/registro/calendario4', [
           // 'model' => $model,
           'clases'=>$clases,
           'pcs'=>$pcs,
           'cursos'=>$cursos,
           'horas'=>$horas,

        ]);
    }
        if(isset($clases[0]->hora_start) && $clases[0]->hora_start=="14:30"){
        return $this->render('/registro/calendario4', [
           // 'model' => $model,
           'clases'=>$clases,
           'pcs'=>$pcs,
           'cursos'=>$cursos,
           'horas'=>$horas
        ]);
    }
        if( isset($clases[0]->hora_start) && $clases[0]->hora_start=="15:20"){
        return $this->render('/registro/calendario4', [
           // 'model' => $model,
           'clases'=>$clases,
           'pcs'=>$pcs,
           'cursos'=>$cursos,
           'horas'=>$horas
        ]);
    }

    return $this->render('/registro/calendario4', [
        // 'model' => $model,
        'clases'=>$clases,
        'pcs'=>$pcs,
        'horas'=>$horas
     ]);


    }
}

