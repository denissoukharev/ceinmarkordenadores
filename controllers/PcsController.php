<?php

namespace app\controllers;

use app\models\Pcs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PcsController implements the CRUD actions for Pcs model.
 */
class PcsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Pcs models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Pcs::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigopc' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pcs model.
     * @param int $codigopc Codigopc
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigopc)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigopc),
        ]);
    }

    /**
     * Creates a new Pcs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pcs();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigopc' => $model->codigopc]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pcs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigopc Codigopc
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigopc)
    {
        $model = $this->findModel($codigopc);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigopc' => $model->codigopc]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pcs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigopc Codigopc
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigopc)
    {
        $this->findModel($codigopc)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pcs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigopc Codigopc
     * @return Pcs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigopc)
    {
        if (($model = Pcs::findOne(['codigopc' => $codigopc])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
