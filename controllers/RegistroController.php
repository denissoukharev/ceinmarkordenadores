<?php

namespace app\controllers;

use app\models\Registro;
use app\models\Clases;
use app\models\Cursos;
use app\models\Horario;
use app\models\Pcs;
use app\models\RegistroSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegistroController implements the CRUD actions for Registro model.
 */
class RegistroController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                      
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Registro models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RegistroSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Registro model.
     * @param int $registroid Registroid
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($registroid)
    {
        return $this->render('view', [
            'model' => $this->findModel($registroid),
        ]);
    }

    /**
     * Creates a new Registro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Registro();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'registroid' => $model->registroid]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCreatemodal()
    {
        $model = new Registro();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['calendario', 'curso' => $model->curso]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Registro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $registroid Registroid
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($registroid)
    {
        $model = $this->findModel($registroid);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'registroid' => $model->registroid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Registro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $registroid Registroid
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($registroid)
    {
        $this->findModel($registroid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Registro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $registroid Registroid
     * @return Registro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($registroid)
    {
        if (($model = Registro::findOne(['registroid' => $registroid])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    public function actionCalendario($curso)
    {
       /* $model = $this->findModel($registroid);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'registroid' => $model->registroid]);
        }*/
        $horas=Horario::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $clases=Clases::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $pcs=Registro::find()->where(['curso'=>$curso])->orderBy(['hora_man_str'=>'ASC'])->all();
        $cursos=Cursos::find()->all();
        
        if( isset($clases[0]->hora_start) && $clases[0]->hora_start=="08:30"){
        return $this->render('calendario4', [
           // 'model' => $model,
           'clases'=>$clases,
           'pcs'=>$pcs,
           'cursos'=>$cursos,
           'horas'=>$horas,

        ]);
    }
        if(isset($clases[0]->hora_start) && $clases[0]->hora_start=="14:30"){
        return $this->render('calendario4', [
           // 'model' => $model,
           'clases'=>$clases,
           'pcs'=>$pcs,
           'cursos'=>$cursos,
           'horas'=>$horas,
        ]);
    }
        if( isset($clases[0]->hora_start) && $clases[0]->hora_start=="15:20"){
        return $this->render('calendario4', [
           // 'model' => $model,
           'clases'=>$clases,
           'pcs'=>$pcs,
           'cursos'=>$cursos,
           'horas'=>$horas,
        ]);
    }

    return $this->render('calendario4', [
        // 'model' => $model,
        'clases'=>$clases,
        'pcs'=>$pcs,
        'cursos'=>$cursos,
        'horas'=>$horas,
     ]);


    }

    public function actionRegpc($curso,$hora_start,$hora_end,$dia){
        $horas=Horario::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $pcs=Registro::find()->where(['curso'=>$curso])->andWhere(['hora_man_str'=>$hora_start])->andWhere(['hora_tar_end'=>$dia])->orderBy(['codigopc'=>'ASC'])->all();
        $codigos=Pcs::find()->orderBy(['codigopc'=>'ASC'])->all();
        $curso=$curso;
        $hora_start=$hora_start;
        $hora_end=$hora_end;
        $dia=$dia;


        return $this->render('regpc',[
           
            'pcs'=>$pcs,
            'codigos'=>$codigos,
            'curso'=>$curso,
            'hora_start'=>$hora_start,
            'hora_end'=>$hora_end,
            'dia'=>$dia,
        ]);
    }



    public function actionCreateadd($codigopc,$serie,$clase,$curso,$hora_man_str,$hora_man_end,$hora_tar_end)
    {




        $cd = new Registro();
        $cd->codigopc=$codigopc;
        $cd->serie=$serie;
        $cd->clase=$clase;
        $cd->curso=$curso;
        $cd->hora_man_str=$hora_man_str;
        $cd->hora_man_end=$hora_man_end;
        $cd->hora_tar_end=$hora_tar_end;
        $cd->save();

        $horas=Horario::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $pcs=Registro::find()->where(['curso'=>$curso])->andWhere(['hora_man_str'=>$hora_man_str])->andWhere(['hora_tar_end'=>$hora_tar_end])->orderBy(['codigopc'=>'ASC'])->all();
        $codigos=Pcs::find()->orderBy(['codigopc'=>'ASC'])->all();

        return $this->render('regpc',[
           'horas'=>$horas,
           'pcs'=>$pcs,
           'codigos'=>$codigos,
            
        ]);
       

       
    }


    public function actionDeletebut($registroid,$curso,$hora_man_str,$hora_tar_end)
    {
        $this->findModel($registroid)->delete();

        $horas=Horario::find()->where(['curso'=>$curso])->orderBy(['hora_start'=>'ASC'])->all();
        $pcs=Registro::find()->where(['curso'=>$curso])->andWhere(['hora_man_str'=>$hora_man_str])->andWhere(['hora_tar_end'=>$hora_tar_end])->orderBy(['codigopc'=>'ASC'])->all();
        $codigos=Pcs::find()->orderBy(['codigopc'=>'ASC'])->all();

        return $this->render('regpc',[
           'horas'=>$horas,
           'pcs'=>$pcs,
           'codigos'=>$codigos,
            
        ]);
    }
}
