-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2023 at 11:10 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ceinmark_ordenadores`
--

-- --------------------------------------------------------

--
-- Table structure for table `estado`
--

CREATE TABLE `estado` (
  `idprestamo` int(11) NOT NULL,
  `codigopc` int(11) DEFAULT NULL,
  `serie` int(11) DEFAULT NULL,
  `usuario` varchar(250) DEFAULT NULL,
  `nota` varchar(250) DEFAULT NULL,
  `estado_manana` varchar(25) DEFAULT NULL,
  `estado_tarde` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `estado`
--

INSERT INTO `estado` (`idprestamo`, `codigopc`, `serie`, `usuario`, `nota`, `estado_manana`, `estado_tarde`) VALUES
(1, 301, 300, 'Diego 2ªAyf', '', 'libre', 'ocupado'),
(2, 302, 300, 'ALBERTO ADMINISTRACIÓN', '', 'no info', 'no info'),
(3, 304, 300, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(4, 305, 300, '2º GAD -  CELIA/1º AYF FERNANDO / Isabel 2º Ayf', '', 'ocupado', 'ocupado'),
(5, 306, 300, '2º GAD - MARIO/ 1º AYF BEGOÑA', '', 'ocupado', 'ocupado'),
(6, 307, 300, '2º GAD -  JOEL/1º AYF ÁLVARO', '', 'ocupado', 'ocupado'),
(7, 308, 300, '2º GAD - HÉCTOR /elisa 2º ayf', '', 'ocupado', 'ocupado'),
(8, 309, 300, '2º GAD - PAULA/ 1º AYF PAULA', '', 'ocupado', 'ocupado'),
(9, 310, 300, '1º AYF JUSTIN /Israel 2º Ayf', '', 'libre', 'ocupado'),
(10, 201, 200, '', 'Desechado', 'no info', 'no info'),
(11, 202, 200, 'Paula', '', 'ocupado', 'ocupado'),
(12, 401, 400, '', 'Básica', 'no info', 'no info'),
(13, 402, 400, '1º AYF MARÍA SAGRARIO/básica ', '', 'libre', 'ocupado'),
(14, 403, 400, '1º AYF FERNANDO/Básica ', '', 'libre', 'ocupado'),
(16, 405, 400, '1º AYF ROBERTO/básica', '', 'libre', 'ocupado'),
(17, 406, 400, '1º AYF TATIANA DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'libre', 'ocupado'),
(18, 407, 400, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'no info', 'no info'),
(19, 408, 400, '1º AYF MELISA DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'ocupado'),
(20, 409, 400, 'DAVID', ' (MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(21, 410, 400, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(22, 411, 400, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(23, 501, 500, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(24, 502, 500, 'DAVID  ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(25, 503, 500, 'DAVID  ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(26, 504, 500, 'DAVID', '', 'no info', 'no info'),
(27, 505, 500, 'JESICA', '', 'no info', 'no info'),
(28, 601, 600, '2º DAM. JUAN', '', 'ocupado', 'libre'),
(29, 602, 600, '2º DAM. ÁLVARO', '', 'ocupado', 'libre'),
(30, 603, 600, '2º DAM. DIEGO', '', 'ocupado', 'libre'),
(31, 604, 600, '2º DAM. FERNANDO', '', 'ocupado', 'libre'),
(32, 605, 600, '2º DAM. MATEO', '', 'ocupado', 'libre'),
(33, 606, 600, 'PRACTICAS ALPE', '', 'ocupado', 'ocupado'),
(34, 607, 600, '', '', 'no info', 'no info'),
(35, 608, 600, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(36, 609, 600, 'DAVID ', ' (MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(37, 610, 600, '1º DAM. JOSELYN', '', 'ocupado', 'libre'),
(38, 611, 600, '', '', 'no info', 'no info'),
(39, 612, 600, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(40, 613, 600, '', 'EXTRAVIADO', 'no info', 'no info'),
(41, 614, 600, 'DAVID', ' (MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(42, 615, 600, 'PRACTICAS ALPE', '', 'ocupado', 'ocupado'),
(43, 616, 600, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(44, 617, 600, 'Cristina. Aux Enfermería', '', 'no info', 'no info'),
(45, 618, 600, '1º DAM. YOEL', '', 'ocupado', 'libre'),
(46, 619, 600, '1º DAM. CLAUDIA', '', 'ocupado', 'libre'),
(47, 620, 600, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(48, 621, 600, '1º DAM- CONSTANTINE', '', 'ocupado', 'libre'),
(49, 622, 600, 'Rocio. Aux Enfermería', '', 'no info', 'no info'),
(50, 623, 600, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(51, 624, 600, 'DAVID ', '(MAÑANAS HASTA FEBRERO ROMANES)', 'ocupado', 'libre'),
(52, 625, 600, 'Irene. Aux Enfermeria', '', 'no info', 'no info'),
(54, 101, 100, 'anna', '', 'libre', 'ocupado');

-- --------------------------------------------------------

--
-- Table structure for table `pcs`
--

CREATE TABLE `pcs` (
  `codigopc` int(11) NOT NULL,
  `serie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pcs`
--

INSERT INTO `pcs` (`codigopc`, `serie`) VALUES
(101, 100),
(102, 100),
(103, 100),
(104, 100),
(107, 100),
(108, 100),
(109, 100),
(110, 100),
(201, 200),
(202, 200),
(301, 300),
(302, 300),
(304, 300),
(305, 300),
(306, 300),
(307, 300),
(308, 300),
(309, 300),
(310, 300),
(401, 400),
(402, 400),
(403, 400),
(405, 400),
(406, 400),
(407, 400),
(408, 400),
(409, 400),
(410, 400),
(411, 400),
(501, 500),
(502, 500),
(503, 500),
(504, 500),
(505, 500),
(601, 600),
(602, 600),
(603, 600),
(604, 600),
(605, 600),
(606, 600),
(607, 600),
(608, 600),
(609, 600),
(610, 600),
(611, 600),
(612, 600),
(613, 600),
(614, 600),
(615, 600),
(616, 600),
(617, 600),
(618, 600),
(619, 600),
(620, 600),
(621, 600),
(622, 600),
(623, 600),
(624, 600),
(625, 600);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idprestamo`),
  ADD KEY `FK_estado_pcs_codigopc` (`codigopc`);

--
-- Indexes for table `pcs`
--
ALTER TABLE `pcs`
  ADD PRIMARY KEY (`codigopc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `estado`
--
ALTER TABLE `estado`
  MODIFY `idprestamo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `FK_estado_pcs_codigopc` FOREIGN KEY (`codigopc`) REFERENCES `pcs` (`codigopc`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
