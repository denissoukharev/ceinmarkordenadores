<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property int $claseid
 * @property string|null $clase
 * @property string|null $curso
 * @property string|null $hora_start
 * @property string|null $hora_end
 * @property string|null $dias
 */
class Clases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clase', 'hora_start', 'hora_end'], 'string', 'max' => 25],
            [['curso', 'dias'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'claseid' => 'Claseid',
            'clase' => 'Clase',
            'curso' => 'Curso',
            'hora_start' => 'Hora Start',
            'hora_end' => 'Hora End',
            'dias' => 'Dias',
        ];
    }
}
