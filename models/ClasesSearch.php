<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clases;

/**
 * ClasesSearch represents the model behind the search form of `app\models\Clases`.
 */
class ClasesSearch extends Clases
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['claseid'], 'integer'],
            [['clase', 'curso', 'hora_start', 'hora_end', 'dias'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clases::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'claseid' => $this->claseid,
        ]);

        $query->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'curso', $this->curso])
            ->andFilterWhere(['like', 'hora_start', $this->hora_start])
            ->andFilterWhere(['like', 'hora_end', $this->hora_end])
            ->andFilterWhere(['like', 'dias', $this->dias]);

        return $dataProvider;
    }
}
