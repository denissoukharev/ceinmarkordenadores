<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cursos".
 *
 * @property int $cursoid
 * @property string|null $curso
 * @property string|null $fecha_start
 * @property string|null $fecha_end
 */
class Cursos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cursos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['curso'], 'string', 'max' => 50],
            [['fecha_start', 'fecha_end'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cursoid' => 'Cursoid',
            'curso' => 'Curso',
            'fecha_start' => 'Fecha Start',
            'fecha_end' => 'Fecha End',
        ];
    }
}
