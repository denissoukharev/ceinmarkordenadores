<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado".
 *
 * @property int $idprestamo
 * @property int|null $codigopc
 * @property int|null $serie
 * @property string|null $usuario
 * @property string|null $nota
 * @property string|null $estado_manana
 * @property string|null $estado_tarde
 *
 * @property Pcs $codigopc0
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigopc', 'serie'], 'integer'],
            [['usuario', 'nota'], 'string', 'max' => 250],
            [['estado_manana', 'estado_tarde'], 'string', 'max' => 25],
            [['codigopc'], 'exist', 'skipOnError' => true, 'targetClass' => Pcs::class, 'targetAttribute' => ['codigopc' => 'codigopc']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprestamo' => 'Idprestamo',
            'codigopc' => 'Codigopc',
            'serie' => 'Serie',
            'usuario' => 'Usuario',
            'nota' => 'Observaciones',
            'estado_manana' => 'Estado Manana',
            'estado_tarde' => 'Estado Tarde',
        ];
    }

    /**
     * Gets query for [[Codigopc0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigopc0()
    {
        return $this->hasOne(Pcs::class, ['codigopc' => 'codigopc']);
    }
}
