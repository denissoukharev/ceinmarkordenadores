<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "horario".
 *
 * @property string|null $curso
 * @property string|null $hora_start
 * @property string|null $hora_end
 */
class Horario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'horario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['curso', 'hora_start', 'hora_end'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'curso' => 'Curso',
            'hora_start' => 'Hora Start',
            'hora_end' => 'Hora End',
        ];
    }
}
