<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registro".
 *
 * @property int $registroid
 * @property int|null $codigopc
 * @property int|null $serie
 * @property string|null $usuario
 * @property string|null $clase
 * @property string|null $curso
 * @property string|null $hora_man_str
 * @property string|null $hora_man_end
 * @property string|null $hora_tar_str
 * @property string|null $hora_tar_end
 * @property string|null $manana_libre
 * @property string|null $manana_ocupado
 * @property string|null $tarde_libre
 * @property string|null $tarde_ocupado
 */
class Registro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigopc', 'serie'], 'integer'],
            [['usuario', 'clase', 'curso', 'hora_man_str', 'hora_man_end', 'hora_tar_str', 'hora_tar_end', 'manana_libre', 'manana_ocupado', 'tarde_libre', 'tarde_ocupado'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'registroid' => 'Registroid',
            'codigopc' => 'Codigopc',
            'serie' => 'Serie',
            'usuario' => 'Usuario',
            'clase' => 'Clase',
            'curso' => 'Curso',
            'hora_man_str' => 'Hora Inicio',
            'hora_man_end' => 'Hora Fin',
            'hora_tar_str' => 'Observaciones',
            'hora_tar_end' => 'Dia',
            'manana_libre' => 'Manana Libre',
            'manana_ocupado' => 'Manana Ocupado',
            'tarde_libre' => 'Tarde Libre',
            'tarde_ocupado' => 'Tarde Ocupado',
        ];
    }

    public function libreman(){

       
    }
    public function ocupman(){
     return "ocupado: " . $this->hora_man_str ."-" . $this->hora_man_end;
       
    }
}
