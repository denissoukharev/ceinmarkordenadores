<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Registro;

/**
 * RegistroSearch represents the model behind the search form of `app\models\Registro`.
 */
class RegistroSearch extends Registro
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registroid', 'codigopc', 'serie'], 'integer'],
            [['usuario', 'clase', 'curso', 'hora_man_str', 'hora_man_end', 'hora_tar_str', 'hora_tar_end', 'manana_libre', 'manana_ocupado', 'tarde_libre', 'tarde_ocupado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Registro::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'registroid' => $this->registroid,
            'codigopc' => $this->codigopc,
            'serie' => $this->serie,
        ]);

        $query->andFilterWhere(['like', 'usuario', $this->usuario])
            ->andFilterWhere(['like', 'clase', $this->clase])
            ->andFilterWhere(['like', 'curso', $this->curso])
            ->andFilterWhere(['like', 'hora_man_str', $this->hora_man_str])
            ->andFilterWhere(['like', 'hora_man_end', $this->hora_man_end])
            ->andFilterWhere(['like', 'hora_tar_str', $this->hora_tar_str])
            ->andFilterWhere(['like', 'hora_tar_end', $this->hora_tar_end])
            ->andFilterWhere(['like', 'manana_libre', $this->manana_libre])
            ->andFilterWhere(['like', 'manana_ocupado', $this->manana_ocupado])
            ->andFilterWhere(['like', 'tarde_libre', $this->tarde_libre])
            ->andFilterWhere(['like', 'tarde_ocupado', $this->tarde_ocupado]);

        return $dataProvider;
    }
}
