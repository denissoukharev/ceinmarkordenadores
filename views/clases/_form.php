<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cursos;

/** @var yii\web\View $this */
/** @var app\models\Clases $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clases-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clase')->textInput(['maxlength' => true]) ?>

    <?php $cr = \yii\helpers\ArrayHelper::map(Cursos::find()->all(), 'curso', 'curso'); ?>
    <?= $form->field($model, 'curso')->dropDownList($cr, ['prompt' => 'Seleccione Uno' ]); ?>

    <?= $form->field($model, 'hora_start')->Input('time')  ?>

    <?= $form->field($model, 'hora_end')->Input('time')  ?>

    <?= $form->field($model, 'dias')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
