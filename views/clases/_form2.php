<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cursos;

/** @var yii\web\View $this */
/** @var app\models\Clases $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clases-form">
<?php $form = ActiveForm::begin(); ?>

<table>
        <tr>
            <td>Clase</td>
            <td>Curso</td>
            <td>Hora Start</td>
            <td>Hora End</td>
            <td>Dia</td>
        </tr>
        <?php 
       

            
            ?>       
        <tr>            
            <td><?= $form->field($model, 'clase')->textInput(['maxlength' => true])->label(FALSE) ?></td>
            <td><?= $form->field($model, 'curso')->textInput(['class' => 'cursoadd form-control'])->label(FALSE) ?></td>
            <td><?= $form->field($model, 'hora_start')->textInput(['maxlength' => true,'class' => 'horastart form-control'])->label(FALSE) ?></td>  
            <td><?= $form->field($model, 'hora_end')->textInput(['maxlength' => true,'class' => 'horaend form-control'])->label(FALSE) ?></td>  
            <td><?= $form->field($model, 'dias')->textInput(['maxlength' => true,'class' => 'dias form-control'])->label(FALSE) ?></td>  
                 
        </tr> 
       <?php  ?>
    </table>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
