<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ClasesSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="clases-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'claseid') ?>

    <?= $form->field($model, 'clase') ?>

    <?= $form->field($model, 'curso') ?>

    <?= $form->field($model, 'hora_start') ?>

    <?= $form->field($model, 'hora_end') ?>

    <?php // echo $form->field($model, 'dias') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
