<?php

use app\models\Clases;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\ClasesSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clases-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Clases', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'claseid',
            'clase',
            'curso',
            'hora_start',
            'hora_end',
            'dias',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Clases $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'claseid' => $model->claseid]);
                 }
            ],
        ],
    ]); ?>


</div>
