<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Clases $model */

$this->title = 'Update Clases: ' . $model->claseid;
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->claseid, 'url' => ['view', 'claseid' => $model->claseid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clases-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
