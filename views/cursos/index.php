<?php

use app\models\Cursos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\CursosSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Cursos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cursos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Cursos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cursoid',
            'curso',
            'fecha_start',
            'fecha_end',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Cursos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cursoid' => $model->cursoid]);
                 }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                  'header' => 'Actions',
                  'headerOptions' => ['style' => 'color:#337ab7'],
                  'template'=>" {calendario}",
                  'buttons' => [
                  'calendario' => function ($url, $model) {
                      return Html::a(' horario', $url, [
                                  'title' => Yii::t('app', 'horario'),
                      ]);
                    },
                    
  
              ],
              'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'calendario') {
                    $url =Url::toRoute([$action, 'curso' => $model->curso]);
                    return $url;
                }},
             
          ],

            
        ],
    ]); ?>


</div>
