<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Estado $model */

$this->title = $model->idprestamo;
$this->params['breadcrumbs'][] = ['label' => 'Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idprestamo' => $model->idprestamo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idprestamo' => $model->idprestamo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idprestamo',
            'codigopc',
            'serie',
            'usuario',
            'nota',
            'estado_manana',
            'estado_tarde',
        ],
    ]) ?>

</div>
