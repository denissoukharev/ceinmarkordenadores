<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/** @var yii\web\View $this */
/** @var app\models\Horario $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="horario-form">

    <?php $form = ActiveForm::begin(); ?>

    <table>
        <tr>
            <td>Curso</td>
            <td>Hora Start</td>
            <td>Hora End</td>
        </tr>
        <?php 
        $options = ['class' => 'btn btn-default'];
        foreach ($prts as $index => $product) { 
            $counter=$count;
            if( $counter==1){
                $men=1;
                $mas=$counter+1;
            }else{
            $mas=$counter+1;
            $men=$counter-1;
            }

            
            ?>       
        <tr>            
            <td><?= $form->field($product, "[$index]curso")->textInput(['maxlength' => true])->label(FALSE) ?></td>
            <td><?= $form->field($product, "[$index]hora_start")->textInput()->label(FALSE) ?></td>
            <td><?= $form->field($product, "[$index]hora_end")->textInput(['maxlength' => true])->label(FALSE) ?></td>  
            <td><?= Html::a('<i class="fa-solid fa-plus"></i>', ['/horario/createdos?c=' . $mas], ['class'=>' btn btn-success']) ?></td> 
               
            <td><?= Html::a('<i class="fa-solid fa-x"></i>', ['/horario/createdos?c=' . $men], ['class'=>'btn btn-danger']) ?></td>      
        </tr> 
       <?php } ?>
    </table>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
