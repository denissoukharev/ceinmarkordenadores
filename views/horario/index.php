<?php

use app\models\Horario;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\models\HorarioSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Horarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Horario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'curso',
            'hora_start',
            'hora_end',
            'horarioid',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Horario $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'horarioid' => $model->horarioid]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
