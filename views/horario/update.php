<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Horario $model */

$this->title = 'Update Horario: ' . $model->horarioid;
$this->params['breadcrumbs'][] = ['label' => 'Horarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->horarioid, 'url' => ['view', 'horarioid' => $model->horarioid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="horario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form2', [
        'model' => $model,
    ]) ?>

</div>
