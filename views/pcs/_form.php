<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pcs $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pcs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigopc')->textInput() ?>

    <?= $form->field($model, 'serie')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
