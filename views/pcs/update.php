<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pcs $model */

$this->title = 'Actualizar PC: ' . $model->codigopc;
$this->params['breadcrumbs'][] = ['label' => 'PCs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigopc, 'url' => ['view', 'codigopc' => $model->codigopc]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="pcs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
