<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Clases;
use app\models\Cursos;

/** @var yii\web\View $this */
/** @var app\models\Registro $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="registro-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigopc')->textInput() ?>

    <?= $form->field($model, 'serie')->textInput() ?>

    <?= $form->field($model, 'usuario')->textInput(['maxlength' => true]) ?>
       <?php $cl = \yii\helpers\ArrayHelper::map(Clases::find()->all(), 'clase', 'clase'); ?>
    <?= $form->field($model, 'clase')->dropDownList($cl, ['prompt' => 'Seleccione Uno' ]); ?>
    <?php $cr = \yii\helpers\ArrayHelper::map(Cursos::find()->all(), 'curso', 'curso'); ?>
    <?= $form->field($model, 'curso')->dropDownList($cr, ['prompt' => 'Seleccione Uno' ]); ?>

    <?= $form->field($model, 'hora_man_str')->Input('time') ?>

    <?= $form->field($model, 'hora_man_end')->Input('time')  ?>

    <?= $form->field($model, 'hora_tar_str')->textInput(['maxlength' => true]) ?>
    <?php $var = [ 'lunes' => 'lunes', 'martes' => 'martes','miercoles' => 'miercoles','jueves' => 'jueves','viernes' => 'viernes']; ?>

    <?= $form->field($model, 'hora_tar_end')->dropDownList($var, ['prompt' => 'Seleccione Uno' ]); ?>

  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
