<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\RegistroSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="registro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= // $form->field($model, 'registroid') ?>

    <?= $form->field($model, 'codigopc') ?>

    <?= $form->field($model, 'serie') ?>

    <?= $form->field($model, 'usuario') ?>

    <?= $form->field($model, 'clase') ?>

    <?php  echo $form->field($model, 'curso') ?>

    <?php // echo $form->field($model, 'hora_man_str') ?>

    <?php // echo $form->field($model, 'hora_man_end') ?>

    <?php // echo $form->field($model, 'hora_tar_str') ?>

    <?php // echo $form->field($model, 'hora_tar_end') ?>

    <?php  echo $form->field($model, 'manana_libre') ?>

    <?php echo $form->field($model, 'manana_ocupado') ?>

    <?php echo $form->field($model, 'tarde_libre') ?>

    <?php  echo $form->field($model, 'tarde_ocupado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
