


<?php 
use app\models\Registro;
use app\models\Cursos;
use yii\helpers\Html;
use yii\bootstrap5\Modal;
use yii\helpers\Url;


$registro=Registro::find()->all();


?>


<?php if(isset($clases[0]->curso)){?>
  
 <H1> Curso: <?= $clases[0]->curso?></H1>
 
 <p>
        
       <?php  foreach($cursos as $cur){
                  ?> <a href="calendario?curso=<?= $cur->curso ?>" class="btn btn-info" role="button"><?= $cur->curso ?></a>
      <?php } ?>
      
           
         
    </p>
 <p>
        <?= Html::button('Añadir clase',  ['value'=>Url::to('@web/index.php/clases/createmodal'),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        <?= Html::button('Añadir curso',  ['value'=>Url::to('@web/index.php/cursos/createmodal'),'class' => 'btn btn-success','id'=>'modalButtondos']) ?>
        <?= Html::button('Añadir pc',  ['value'=>Url::to('@web/index.php/registro/createmodal'),'class' => 'btn btn-success','id'=>'modalButtontres']) ?>
    </p>

    <?php 
Modal::begin([
    'title'=>'<h4>Clases</h4>',
    'id'=>'modal',
    'size'=>'modal-lg',
]);

echo "<div id='modalContent'></div>";

Modal::end();
Modal::begin([
    'title'=>'<h4>Cursos</h4>',
    'id'=>'modaldos',
    'size'=>'modal-lg',
]);

echo "<div id='modalContentdos'></div>";

Modal::end();

Modal::begin([
    'title'=>'<h4>Registro</h4>',
    'id'=>'modaltres',
    'size'=>'modal-lg',
]);

echo "<div id='modalContenttres'></div>";

Modal::end();

Modal::begin([
  'title'=>'<h4>Pcs</h4>',
  'id'=>'modalcuatro',
  'size'=>'modal-lg',
]);

echo "<div id='modalContentcuatro'></div>";

Modal::end();
Modal::begin([
  'title'=>'<h4>Pcs</h4>',
  'id'=>'modalcodigo',
  'size'=>'modal-lg',
]);

echo "<div id='modalContentcodigo'></div>";

Modal::end();



?>

<TABLE class="myTimetable" id="horario"> 
  <THEAD>
    <TR>
      <TH></TH>
      <TH>Lunes</TH>
      <TH>Martes</TH>
      <TH>Miercoles</TH>
      <TH>Jueves</TH>
      <TH>Viernes</TH>     
    </TR>
  </THEAD>
  <TBODY>
  
    <TR>
      <TD>08:30-9:20
      
      </TD>
    
      <TD>
      <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc?curso=' . $clases[0]->curso . '&hora=08:30&dia=lunes'),'class' => 'btn btn-success no-print','id'=>'modalButtoncuatro']) ?></p>
      <?php 
      
      
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="lunes" && $clase->hora_start=="08:30"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
       
        <div class="room">Ocupados:<br><?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="08:30"  && $pc->hora_tar_end=="lunes"){
          
          echo  $pc->codigopc . "," ;
          }
          
          } ?></div>
        <!-- <div class="room">Libres:<?php /*foreach($pcs as $pc){ 
          
          if($pc->hora_man_str="08:30"  && $pc->hora_tar_end=="lunes" && $pc->curso!=$clases[0]->curso){
          
          echo  $pc->codigopc . ",";
          }
          
          }
         
          
          
          
          */?></div> -->
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
     
      
    }
         ?>
      </TD>
      <TD>
      <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc'),'class' => 'btn btn-success codigo']) ?></p>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="martes" && $clase->hora_start=="08:30"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="08:30" && $pc->hora_tar_end=="martes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }

     
    }
         ?>
      </TD>
      <TD>
      <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc'),'class' => 'btn btn-success codigo ']) ?></p>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="miercoles" && $clase->hora_start=="08:30"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="08:30" && $pc->hora_tar_end=="miercoles") {
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="jueves" && $clase->hora_start=="08:30"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="08:30" && $pc->hora_tar_end=="jueves" ){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="viernes" && $clase->hora_start=="08:30"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="08:30" && $pc->hora_tar_end=="viernes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>    
    </TR>
    <TR>
      <TD>09:20-10:10</TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="lunes" && $clase->hora_start=="09:20"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="09:20" && $pc->hora_tar_end=="lunes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="martes" && $clase->hora_start=="09:20"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="09:20" && $pc->hora_tar_end=="martes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="miercoles" && $clase->hora_start=="09:20"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="09:20" && $pc->hora_tar_end=="miercoles"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="jueves" && $clase->hora_start=="09:20"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="09:20" && $pc->hora_tar_end=="jueves"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="viernes" && $clase->hora_start=="09:20"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="09:20" && $pc->hora_tar_end=="viernes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
    </TR>
    <TR>
      <TD>10:10-11:00</TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="lunes" && $clase->hora_start=="10:10"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="10:10" && $pc->hora_tar_end=="lunes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="martes" && $clase->hora_start=="10:10"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="10:10" && $pc->hora_tar_end=="martes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="miercoles" && $clase->hora_start=="10:10"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="10:10" && $pc->hora_tar_end=="miercoles"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="jueves" && $clase->hora_start=="10:10"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="10:10" && $pc->hora_tar_end=="jueves"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="viernes" && $clase->hora_start=="10:10"){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str=="10:10" && $pc->hora_tar_end=="viernes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
    </TR>
      <TR>
      <TD colspan="6" class="break">Descanso</TD>
      </TR>
      <TR>
      <TD>11:30-12:20</TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="lunes" && $clase->hora_start=="11:30"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="11:30" && $pc->hora_tar_end=="lunes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="martes" && $clase->hora_start=="11:30"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="11:30" && $pc->hora_tar_end=="martes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="miercoles" && $clase->hora_start=="11:30"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="11:30" && $pc->hora_tar_end=="miercoles"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="jueves" && $clase->hora_start=="11:30"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="11:30" && $pc->hora_tar_end=="jueves"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="viernes" && $clase->hora_start=="11:30"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="11:30" && $pc->hora_tar_end=="viernes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
   
      <TR>
      <TD>12:20-13:10</TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="lunes" && $clase->hora_start=="12:20"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="12:20" && $pc->hora_tar_end=="lunes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="martes" && $clase->hora_start=="12:20"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="12:20" && $pc->hora_tar_end=="martes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="miercoles" && $clase->hora_start=="12:20"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="12:20" && $pc->hora_tar_end=="miercoles"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="jueves" && $clase->hora_start=="12:20"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="12:20" && $pc->hora_tar_end=="jueves"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="viernes" && $clase->hora_start=="12:20"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="12:20" && $pc->hora_tar_end=="viernes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
    </TR>
      <TR>
      <TD>13:10-14:00</TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="lunes" && $clase->hora_start=="13:10"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="13:10" && $pc->hora_tar_end=="lunes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="martes" && $clase->hora_start=="13:10"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="13:10" && $pc->hora_tar_end=="martes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="miercoles" && $clase->hora_start=="13:10"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="13:10" && $pc->hora_tar_end=="miercoles"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="jueves" && $clase->hora_start=="13:10"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="13:10" && $pc->hora_tar_end=="jueves"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
      <TD>
      <?php 
      $hora=$clases[0]->hora_start;
      foreach($clases as $clase) { ?>
      <?php  if( $clase->dias=="viernes" && $clase->hora_start=="13:10"){?>
         
        <div class="subject"><?= $clase->clase ?></div>
      
        <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=="13:10" && $pc->hora_tar_end=="viernes"){
          
          echo  $pc->codigopc . ",";
          }
          
          } ?></div>
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
      </TD>
    </TR>
   </TBODY>
  </TABLE>
  <button id="btn-print-this" class="btn btn-success btn-lg">Imprimir</button>


  <?php }else{
  ?>
  
  <H1> Rellena la tablas de cursos y clases primero</H1>
  <p>
        <?= Html::button('Añadir clase',  ['value'=>Url::to('@web/index.php/clases/createmodal'),'class' => 'btn btn-success modalButton',]) ?>
        <?= Html::button('Añadir curso',  ['value'=>Url::to('@web/index.php/cursos/createmodal'),'class' => 'btn btn-success modalButtondos',]) ?>
        <?= Html::button('Añadir pc',  ['value'=>Url::to('@web/index.php/registro/createmodal'),'class' => 'btn btn-success modalButtontres',]) ?>
    </p>

  <TABLE class="myTimetable"> 
  <THEAD>
    <TR>
      <TH></TH>
      <TH>Lunes</TH>
      <TH>Martes</TH>
      <TH>Miercoles</TH>
      <TH>Jueves</TH>
      <TH>Viernes</TH>     
    </TR>
  </THEAD>
  <TBODY>
    <TR>
      <TD>08:30-09:20</TD>
      <TD>
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
       <TD>        
         <div class="subject"></div>
        <div class="room"></div>
      </TD>     
    </TR>
    <TR>
      <TD>09:20-10:10</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
    <TR>
      <TD>10:10-11:00</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
      <TR>
       
      <TD colspan="6" class="break">Descanso</TD>
      </TR>
      <TR>
      <TD>11:30-12:20</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
   
      <TR>
      <TD>12:20-13:10</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
      <TR>
      <TD>13:10-14:00</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
   </TBODY>
  </TABLE>
  

  <?php }  ?>


  <?php
   Modal::begin([
    'title'=>'<h4>Registro</h4>',
    'id'=>'modalcuatro',
    'size'=>'modal-lg',
]);

echo "<div id='modalContentcuatro'></div>";

Modal::end();
  
  
  
  ?>