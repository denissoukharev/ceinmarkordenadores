


<?php 
use app\models\Registro;
use app\models\Cursos;
use yii\helpers\Html;
use yii\bootstrap5\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;


$registro=Registro::find()->all();


?>


<?php if(isset($clases[0]->curso)){?>
  
 
 
 <p>
        
       <?php  foreach($cursos as $cur){
                  ?> <a href="calendario?curso=<?= $cur->curso ?>" class="btn btn-info" role="button"><?= $cur->curso ?></a>
      <?php } ?>
      
           
         
    </p>
 <p>
        <?= Html::button('Añadir clase',  ['value'=>Url::to('@web/index.php/clases/createmodal'),'class' => 'btn btn-success cursoadd','id'=>'modalButton']) ?>
        <?= Html::button('Añadir curso',  ['value'=>Url::to('@web/index.php/cursos/createmodal'),'class' => 'btn btn-success','id'=>'modalButtondos']) ?>
        <?= Html::button('Añadir pc',  ['value'=>Url::to('@web/index.php/registro/createmodal'),'class' => 'btn btn-success','id'=>'modalButtontres']) ?>
    </p>

    <?php 
Modal::begin([
    'title'=>'<h4>Clases</h4>',
    'id'=>'modal',
    'size'=>'modal-lg',
]);

echo "<div id='modalContent'></div>";

Modal::end();
Modal::begin([
    'title'=>'<h4>Cursos</h4>',
    'id'=>'modaldos',
    'size'=>'modal-lg',
]);

echo "<div id='modalContentdos'></div>";

Modal::end();

Modal::begin([
    'title'=>'<h4>Registro</h4>',
    'id'=>'modaltres',
    'size'=>'modal-lg',
]);

echo "<div id='modalContenttres'></div>";

Modal::end();

Modal::begin([
  'title'=>'<h4>Pcs</h4>',
  'id'=>'modalcuatro',
  'size'=>'modal-lg',
]);

echo "<div id='modalContentcuatro'></div>";

Modal::end();

Modal::begin([
  'title'=>'<h4>Pcs</h4>',
  'id'=>'modalcodigo',
  'size'=>'modal-lg',
]);

echo "<div id='modalContentcodigo'></div>";

Modal::end();



?>

<TABLE class="myTimetable" id="horario"> 
<H1 id="cal" class="curso" value="<?= $clases[0]->curso?>" data-href="calendario?curso=<?= $clases[0]->curso?>"> Curso: <?= $clases[0]->curso?></H1>
  <THEAD>
    <TR>
      <TH>Curso: <?= $clases[0]->curso?></TH>
      <TH>Lunes</TH>
      <TH>Martes</TH>
      <TH>Miercoles</TH>
      <TH>Jueves</TH>
      <TH>Viernes</TH>     
    </TR>
  </THEAD>
  <TBODY>
  

     <?php 
     Pjax::begin(); 
     $indx=0;
     $di=0;
     foreach($horas as $hora){
      $butid=0;
      ?>
    <TR class="hora" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>">
      <TD  class="hora" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" ><?= $hora->hora_start . "-" . $hora->hora_end ?>
      
      </TD>
    
      <TD>
      
      <?php 
      
      
      $h=$hora->hora_start;
      foreach($clases as $clase) { 
        
        ?>
        <h1 class="hora" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" style="display:none;"></h1>

      <?php  if( $clase->dias=="lunes" && $clase->hora_start==$h){?>
         
        <div class="subject dia" dia1="lunes"><?= $clase->clase ?></div>
       
        <div class="room">Ocupados:<br><?php foreach($pcs as $pc){ 
          
          if($pc->hora_man_str==$h  && $pc->hora_tar_end=="lunes"){
          
          echo  $pc->codigopc . "," ;
          }
          
          } ?></div>
        <!-- <div class="room">Libres:<?php /*foreach($pcs as $pc){ 
          
          if($pc->hora_man_str=$h  && $pc->hora_tar_end=="lunes" && $pc->curso!=$clases[0]->curso){
          
          echo  $pc->codigopc . ",";
          }
          
          }
         
          
          
          
          */?></div> -->
        <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
     
      
    }

    
         ?>
         <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc?curso=' . $clases[0]->curso . '&hora_start=' . $hora->hora_start . '&hora_end=' . $hora->hora_end . '&dia=lunes'),'class' => 'btn btn-primary cod no-print  ',]) ?></p>
         <div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" value="/ordenadores2/ceinmark/web/index.php/registro/regpc?curso=<?= $clases[0]->curso ?>&hora_start=<?=$hora->hora_start ?>&hora_end=<?=$hora->hora_end ?>&dia=lunes" class="btn btn-secondary cod no-print ">pcs</button>
   
  <button type="button" dia="lunes" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" value="/ordenadores2/ceinmark/web/index.php/clases/createmodal" class="btn btn-secondary no-print buthora buthora<?= $indx?>">clase</button>
  <?php $indx++; ?>
</div>
      </TD>
      <TD>
     
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="martes" && $clase->hora_start==$h){?>
         
         <div class="subject dia" dia2="martes"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str==$h && $pc->hora_tar_end=="martes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }

     
    }
         ?>
         <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc?curso=abc&hora=' . $hora->hora_start . '&dia=martes'),'class' => 'btn btn-primary cod no-print  ',]) ?></p>
         <div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" value="/ordenadores2/ceinmark/web/index.php/registro/regpc?curso=<?= $clases[0]->curso ?>&hora=<?=$hora->hora_start ?>&dia=martes" class="btn btn-secondary cod no-print ">pcs</button>
   
  <button type="button" dia="martes" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" value="/ordenadores2/ceinmark/web/index.php/clases/createmodal" class="btn btn-secondary no-print buthora buthora<?= $indx?>">clase</button>
  <?php $indx++; ?>
</div>
      </TD>
      <TD>
      
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="miercoles" && $clase->hora_start==$h){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str==$h && $pc->hora_tar_end=="miercoles") {
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
         <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc?curso=abc&hora=08:30&dia=miercoles'),'class' => 'btn btn-primary cod no-print  ',]) ?></p>
         <div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" value="/ordenadores2/ceinmark/web/index.php/registro/regpc?curso=<?= $clases[0]->curso ?>&hora=<?=$hora->hora_start ?>&dia=miercoles" class="btn btn-secondary cod no-print ">pcs</button>
   
  <button type="button" dia="miercoles" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" value="/ordenadores2/ceinmark/web/index.php/clases/createmodal" class="btn btn-secondary no-print buthora buthora<?= $indx?>">clase</button>
  <?php $indx++; ?>
</div>
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="jueves" && $clase->hora_start==$h){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str==$h && $pc->hora_tar_end=="jueves" ){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
         <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc?curso=abc&hora=08:30&dia=jueves'),'class' => 'btn btn-primary cod no-print  ',]) ?></p>
         <div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" value="/ordenadores2/ceinmark/web/index.php/registro/regpc?curso=<?= $clases[0]->curso ?>&hora=<?=$hora->hora_start ?>&dia=jueves" class="btn btn-secondary cod no-print ">pcs</button>
   
  <button type="button" dia="jueves" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" value="/ordenadores2/ceinmark/web/index.php/clases/createmodal" class="btn btn-secondary no-print buthora buthora<?= $indx?>">clase</button>
  <?php $indx++; ?>
</div>
         
      </TD>
      <TD>
      <?php foreach($clases as $clase) { ?>
        <?php  if( $clase->dias=="viernes" && $clase->hora_start==$h){?>
         
         <div class="subject"><?= $clase->clase ?></div>
       
         <div class="room">Ocupados:<?php foreach($pcs as $pc){ 
           
           if($pc->hora_man_str==$h && $pc->hora_tar_end=="viernes"){
           
           echo  $pc->codigopc . ",";
           }
           
           } ?></div>
         <?php
      }else{
      ?>
      
         
        <div class="subject"></div>
      
        <div class="room"></div>
        <?php
    
      }
    }
         ?>
         <p><?= Html::button('PCS',  ['value'=>Url::to('@web/index.php/registro/regpc?curso=abc&hora=08:30&dia=viernes'),'class' => 'btn btn-primary cod no-print  ',]) ?></p>
         <div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" value="/ordenadores2/ceinmark/web/index.php/registro/regpc?curso=<?= $clases[0]->curso ?>&hora=<?=$hora->hora_start ?>&dia=viernes" class="btn btn-secondary cod no-print ">pcs</button>
   
  <button type="button" dia="viernes" horastart="<?=$hora->hora_start ?>" horaend="<?=$hora->hora_end ?>" value="/ordenadores2/ceinmark/web/index.php/clases/createmodal" class="btn btn-secondary no-print buthora buthora<?= $indx?>">clase</button>
  <?php $indx++; ?>
</div>
      </TD>    
    </TR>
  <?php } 
  Pjax::end(); 
  ?>
   
   </TBODY>
  </TABLE>
  <button id="btn-print-this" class="btn btn-success btn-lg">Imprimir</button>


  <?php }else{
  ?>
  
  <H1> Rellena la tablas de cursos y clases primero</H1>
  <p>
        <?= Html::button('Añadir clase',  ['value'=>Url::to('@web/index.php/clases/createmodal'),'class' => 'btn btn-success modalButton',]) ?>
        <?= Html::button('Añadir curso',  ['value'=>Url::to('@web/index.php/cursos/createmodal'),'class' => 'btn btn-success modalButtondos',]) ?>
        <?= Html::button('Añadir pc',  ['value'=>Url::to('@web/index.php/registro/createmodal'),'class' => 'btn btn-success modalButtontres',]) ?>
    </p>

  <TABLE class="myTimetable"> 
  <THEAD>
    <TR>
      <TH></TH>
      <TH>Lunes</TH>
      <TH>Martes</TH>
      <TH>Miercoles</TH>
      <TH>Jueves</TH>
      <TH>Viernes</TH>     
    </TR>
  </THEAD>
  <TBODY>
    <TR>
      <TD>08:30-09:20</TD>
      <TD>
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
       <TD>        
         <div class="subject"></div>
        <div class="room"></div>
      </TD>     
    </TR>
    <TR>
      <TD>09:20-10:10</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
    <TR>
      <TD>10:10-11:00</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
      <TR>
       
      <TD colspan="6" class="break">Descanso</TD>
      </TR>
      <TR>
      <TD>11:30-12:20</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
   
      <TR>
      <TD>12:20-13:10</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
      <TR>
      <TD>13:10-14:00</TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
      <TD>        
        <div class="subject"></div>
        <div class="room"></div>
      </TD>
    </TR>
   </TBODY>
  </TABLE>
  

  <?php }  ?>


  <?php
   Modal::begin([
    'title'=>'<h4>Registro</h4>',
    'id'=>'modalcuatro',
    'size'=>'modal-lg',
]);

echo "<div id='modalContentcuatro'></div>";

Modal::end();
  

  
  ?>