<?php

use app\models\Registro;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;


/** @var yii\web\View $this */
/** @var app\models\RegistroSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Registros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registro-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Registro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php
      $gridColumns=[
        'codigopc',
        'serie',
        'usuario',
        'clase',
        'curso',
        ['attribute'=>'hora_man_str',
        'label'=>'Hora Inicio',
        
       ],
       ['attribute'=>'hora_man_end',
       'label'=>'Hora Fin',
       
      ],
       ['attribute'=>'hora_tar_str',
        'label'=>'Observaciones',
       

       
       ], 
       ['attribute'=>'hora_tar_end',
       'label'=>'Dia',
       
      ],
      ];

      echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns'=>$gridColumns

      ]); 
    ?>

    <div style="
  margin: auto;
  width: 50%;
  border: 3px solid green;
  padding: 10px;
  text-align:center;
  margin-bottom:10px;
  ">Hoy es: <?=  date("d-m-Y");?></div>
 <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'registroid',
            'codigopc',
            'serie',
            'usuario',
            'clase',
            'curso',
            ['attribute'=>'hora_man_str',
            'label'=>'Hora Inicio',
            
           ],
           ['attribute'=>'hora_man_end',
           'label'=>'Hora Fin',
           
          ],
           ['attribute'=>'hora_tar_str',
            'label'=>'Observaciones',
           

           
           ], 
           ['attribute'=>'hora_tar_end',
           'label'=>'Dia',
           
          ],
           /* [
                'attribute'=>'manana_libre',
                'label'=>'Estado Mañana',
                'encodeLabel' => true,
                
                'contentOptions' => function ($model, $key, $index, $column) {
                    if($model->hora_man_str == "08:20")

                    return ['style' => 'background-color:#FF0000; color:#FFFFFF'];  
        
                    if($model->hora_man_str == "libre")
        
                    return ['style' => 'background-color:#00FF00; color:#FFFFFF'];
                    else
                     
                    return ['style' => 'background-color:#808080; color:#FFFFFF'];
                   
                    
                },
                
            ],*/
           /* ['attribute'=>'manana_ocupado',
             'label'=>'Ocupado Mañana',
             'value' => function ($model) {
                return "ocupado: " . $model->hora_man_str ."-" . $model->hora_man_end;
             }
            ],*/
           // 'tarde_libre',
           // 'tarde_ocupado',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Registro $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'registroid' => $model->registroid]);
                 }
            ],  

            [
              'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template'=>" {calendario}",
                'buttons' => [
                'calendario' => function ($url, $model) {
                    return Html::a(' horario', $url, [
                                'title' => Yii::t('app', 'horario'),
                    ]);
                  },
                  

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
              if ($action === 'calendario') {
                  $url =Url::toRoute([$action, 'curso' => $model->curso]);
                  return $url;
              }},
           
        ]],
    ]); ?>

<?php Pjax::end(); ?>
</div>
