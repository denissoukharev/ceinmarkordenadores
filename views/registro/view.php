<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Registro $model */

$this->title = $model->registroid;
$this->params['breadcrumbs'][] = ['label' => 'Registros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="registro-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'registroid' => $model->registroid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'registroid' => $model->registroid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'registroid',
            'codigopc',
            'serie',
            'usuario',
            'clase',
            'curso',
            'hora_man_str',
            'hora_man_end',
            'hora_tar_str',
            'hora_tar_end',
            'manana_libre',
            'manana_ocupado',
            'tarde_libre',
            'tarde_ocupado',
        ],
    ]) ?>

</div>
