<?php

/** @var yii\web\View $this */

$this->title = 'Ceinmark Ordenadores';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">CeinMark Ordenadores</h1>

        <p class="lead">Administracion de prestamos de ordenadores .</p>

        <p><a class="btn btn-lg btn-success" href="/ordenadores2/ceinmark/web/index.php/cursos/index">Crear Curso</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 ">
                <h2>Registro de Codigo PC</h2>

                <p>Añade nuevo PC con su codigo unico en Ordenadores. </p>

                <div class=""><img src="/ordenadores/web/imgs/1.png" alt="" style="max-width:400px; border: 5px solid #555; "></div>
            </div>
            <div class="col-lg-4">
                <h2>Registro de Uso de PC </h2>

                <p>Crea registro de prestamo con un codigo pc , nombre de usuario, nota y disponibilidad de este PC por la mañana y por la tarde.  </p>

                <div><img src="/ordenadores/web/imgs/2.png" alt="" style="max-width:200px; border: 5px solid #555; display: block;
  margin-left: auto;
  margin-right: auto ; "></div>
            </div>
            <div class="col-lg-4">
                <h2>Color Celda</h2>

                <p>En campos Estado Mañana y Estado Tarde, las celdas se pintan en color rojo, si tienen puesto el valor "ocupado". Verde si libre y griz si no  especifica. </p>

                    <div><img src="/ordenadores/web/imgs/3.png" alt="" style="max-width:200px; border: 5px solid #555; display: block;
  margin-left: auto;
  margin-right: auto;"></div>
            </div>
        </div>

    </div>
</div>
